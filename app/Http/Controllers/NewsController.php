<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Berita;
class NewsController extends Controller
{
    public function index()
        {
            $news = Berita::paginate(4);
            return view('new.index', compact('news'));
        }

    public function create()
    {
    	return view('new.create');
    }

    public function create_store(Request $request)
    {
    	$news = new Berita();
    	$news->nama_pengirim = $request->nama_pengirim;
    	$news->jenis_berita = $request->jenis_berita;
    	$news->judul_berita = $request->judul_berita;
    	$news->created_at = $request->tanggal_berita;
    	$news->description = $request->description;
    	$news->save();
    	return redirect('/news');
    }

    public function edit($id)
    {
    	$news = Berita::find($id);
    	return view('new.edit', compact('news'));
    }

    public function edit_store(Request $request, $id)
    {
    	$news = Berita::find($id);

    	$news->nama_pengirim = $request->nama_pengirim;
    	$news->jenis_berita = $request->jenis_berita;
    	$news->judul_berita = $request->judul_berita;
    	$news->created_at = $request->tanggal_berita;
    	$news->description = $request->description;
    	$news->update();
    	return redirect('/news');
    }

    public function delete($id)
    {
    	$news = Berita::find($id);
    	$news->delete();
    	return redirect('/news');
    }
}
