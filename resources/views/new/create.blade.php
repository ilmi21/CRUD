@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create News</div>

                <div class="panel-body">
                <form action="/news/create/store" method="post">
  <div class="form-group">
    <label for="nama_pengirim">Nama Pengirim</label>
    <input type="text" class="form-control" id="nama_pengirim" name="nama_pengirim" >
  </div>

   <div class="form-group">
    <label for="nama_pengirim">Jenis Berita</label>
    <select name="jenis_berita" class="form-control">
        <option>Sport</option>
        <option>Politik</option>
        <option>Ekonomi</option>
         <option>Entertaiment</option>
    </select>
  </div>

   <div class="form-group">
    <label for="judul_berita">Judul Berita</label>
    <input type="text" class="form-control" id="judul_berita" name="judul_berita" >
  </div>

  <div class="form-group">
    <label for="tanggal_berita">Tanggal Berita</label>
    <input type="text" class="form-control" id="tanggal_berita" name="tanggal_berita" placeholder="Year-Month-Date 00:00:00" >
  </div>

   <div class="form-group">
    <label for="description">Description</label>
    <textarea name="description" class="form-control" rows="15"></textarea>
  </div>
  {{csrf_field()}}
  <button type="submit" class="btn btn-success">Submit</button>
</form>   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
