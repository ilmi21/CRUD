@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

        <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">The News</h3>
                </div>
                         <div class="panel-body">
                         <a href="{{route('news.create')}}"><button type="submit" class="btn btn-primary">Create News</button></a>
                         <hr>
                             @foreach($news as $new)
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">{{ $new->judul_berita}} <p class="pull-right">{{$new->created_at}}</p></h3>
                </div>
                         <div class="panel-body">
                             {{ $new->description }}
                        </div>
                        <div class="panel-footer">
                        <a href="/news/edit/{{ $new->id }}"><button type="submit" class="btn btn-primary">Edit</button></a><a href="/news/delete/{{$new->id}}"><button type="submit" class="btn btn-danger">Delete</button></a>
                        </div>

            </div>
             @endforeach
             <?php echo str_replace('/?', '?', $news->render()); ?>
                        <div class="panel-footer">
                        <center>2017</center>
                        </div>

            </div>    


        </div>
    </div>
</div>
@endsection
