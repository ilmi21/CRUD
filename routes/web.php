<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/');

//coba
Route::get('/news', 'NewsController@index')->name('news.index');
Route::get('/news/edit/{id}', 'NewsController@edit')->name('news.edit');
Route::post('/news/edit/{id}/store', 'NewsController@edit_store')->name('news.edit.store');
Route::get('/news/delete/{id}', 'NewsController@delete')->name('news.delete');
Route::get('/news/create', 'NewsController@create')->name('news.create');
Route::post('/news/create/store', 'NewsController@create_store')->name('news.create.store');